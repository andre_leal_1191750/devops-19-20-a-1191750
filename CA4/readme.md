Docker Demo
============

CA4

  Install Docker in you computer and create docker hub account.

## Steps

### 1

Create a local folder in your computer and copy the Dockerfile into that folder.

https://bitbucket.org/atb/docker-compose-spring-tut-demo/src/master/

### 2

On /web/Dockerfile change :
Line 17 - change to your repository like so
```
RUN git clone https://andre_leal_1191750@bitbucket.org/andre_leal_1191750/devops-19-20-a-1191750.git
```
Line19 - change to your gradle file
```
WORKDIR /tmp/build/devops-19-20-a-1191750/CA2/CA2.2
```
Line 21 - give permission to execute
```
RUN chmod +x gradlew
```

### 3

cd into the directory of the file with docker-compose.yml
```
$ docker-compose build
```
Then like it was done with vagrant, to get it to work on the web
```
$ docker-compose up
```

### 3

In the host you can open the spring web application using one of the following options:

  - http://localhost:8080/demo-0.0.1-SNAPSHOT/

You can also open the H2 console using one of the following urls:

- http://localhost:8080/demo-0.0.1-SNAPSHOT/h2-console

For the connection string use: jdbc:h2:tcp://192.168.33.11:9092/./jpadb

### 4 

####Publish the images

Next login into your account of Docker-Hub using the command.

```
$ docker login
```

It asks for username and password. To check the name of the images to be published just perform:
```
$ docker images
```
Web and db images are now shown. Tag the web image using:
```
$ docker tag ca4_web andreleal1191750/devops-19-20-a-1191750:web_spring_boot_application
```
and the db image:
```
$ docker tag ca4_db andreleal1191750/devops-19-20-a-1191750:db_spring_boot_application
```
After this, push the images to the repositoy:
```
$ docker push andreleal1191750/devops-19-20-a-1191750
```

This will allow anyone to use the new created images, using a simple docker pull, like this:
```
$ docker pull andreleal1191750/devops-19-20-a-1191750:web_spring_boot_application
$ docker pull andreleal1191750/devops-19-20-a-1191750:db_spring_boot_application
```
### 5
####Copying Database

Exec to run a shell in the container
```
$ docker-compose exec db /bin/bash
```
Copy the database file to the volume.
```
$ cp ./jpadb.mv.db /usr/src/data
```
To make sure everything worked out properly, list the files in data directory
```
$ ls /usr/src/data –al
```

#Alternative

Kubernetes isn't really an alternative of docker, they actually can function together. 
Docker is a containerization platform, and Kubernetes is a container orchestrator for container platforms like Docker. 
Docker provides packaging and distribution to an application while  kubernetes scales, runs and monitors the application, 
providing an abstraction to make a cluster of machines behave like one big machine, which is vital in a large-scale environment.
Kubernetes actually is more like Docker Swarm (Docker’s own native clustering solution for Docker containers).

This tutorial shows the transformation of a docker application to a kubernetes one.

##Kubernetes

First install minikube and kubectl
```
curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.18.0/bin/windows/amd64/kubectl.exe
```
Add the binary in to your PATH

And check
```
kubectl version --client
```
Next install kompose (Kubernetes + Compose), a conversion tool from docker-compose to kubernetes

###1

cd into the directory of the file with docker-compose.yml
```
kompose up
```
It will not only convert but also build

###2

Same thing as in step 3 of the docker tutorial:

In the host you can open the spring web application using one of the following options:

    http://localhost:8080/demo-0.0.1-SNAPSHOT/

You can also open the H2 console using one of the following urls:

    http://localhost:8080/demo-0.0.1-SNAPSHOT/h2-console

For the connection string use: jdbc:h2:tcp://192.168.33.11:9092/./jpadb

###3

Create a pod (is a group of one or more containers):
```
kubectl create -f pod_ca4_db.yml
kubectl create -f pod_ca4_web.yml
```
Run
```
minikube dashboard
```
Should appear on the browser if searched for the dashboard

###4

Exec to run shell in pod
```
kubectl exec db /bin/bash
```
Copy the database file to the volume.
```
 cp ./jpadb.mv.db /usr/src/data
```
To make sure everything worked out properly, list the files in data directory
```
 ls /usr/src/data –al
```


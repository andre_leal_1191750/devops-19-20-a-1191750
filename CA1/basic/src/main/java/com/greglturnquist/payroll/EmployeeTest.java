package com.greglturnquist.payroll;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {


    Employee andre = new Employee("André", "Leal", "poor", "vagabond", "andre@help.me");

    @Test
    void testEquals() {
        Employee andreIsBack = new Employee("André", "Leal", "poor", "vagabond", "andre@help.me");
        assertEquals(andre, andreIsBack);
    }

    @Test
    void testHashCode() {
        Employee andreIsBack = new Employee("André", "Leal", "poor", "vagabond", "andre@help.me");
        assertEquals(andre.hashCode(), andreIsBack.hashCode());
    }

    @Test
    void getJobTitle() {
        assertEquals("vagabond", andre.getJobTitle());
    }

    @Test
    void setJobTitle() {
        andre.setJobTitle("hired");
        assertEquals("hired", andre.getJobTitle());
    }

    @Test
    void getId() {
        assertNull(andre.getId());
    }

    @Test
    void setFirstName() {
        andre.setFirstName("Francis");
        assertEquals("Francis", andre.getFirstName());
    }

    @Test
    void setLastName() {
        andre.setLastName("Hawk");
        assertEquals("Hawk", andre.getLastName());
    }

    @Test
    void setDescription() {
        andre.setDescription("Rich");
        assertEquals("Rich", andre.getDescription());
    }

    @Test
    void setEmailField() {
        andre.setEmailField("francis@nohelp.needed");
        assertEquals("francis@nohelp.needed", andre.getEmailField());
    }

    @Test
    void constructor() {
        try {
            new Employee("a", "a", ",", null, "");
        }
        catch (NullPointerException e) {
            assertEquals("Fields can't be empty.", e.getMessage());
        }
    }

    @Test
    void constructorEmail() {

        try {
            new Employee("a", "a", "a", "a", "aaa.mail.pt");
        }
        catch (IllegalArgumentException e) {
            assertEquals("Email Field is not valid.", e.getMessage());
        }
    }
}
Git demo
=======

This is a git demo with the objective of an orientation line for version control in Git.

Prerequisites
----------------

 * Install Git
 * Create a personal repository
 * Use cmd line
 
Clone Repository
----------------

Obtain the tutorial data
with :
```
git clone https://github.com/spring-guides/tut-react-and-spring-data-rest
```
push it to your repository
```
git push -u origin -all
```

Tag
---
Tag your repository to help keep up with versions
```
git tag v1.2.0
```
Now push the tag
```
git push origin v1.2.0
```
To help see the tags
```
git tag
```

Branches
-------
 To create a branch
 ```
git branch email-field
```
And to navigate to the branch
```
git checkou email-field
```

Alter the project to add an email field to the application and do some testing

To submit the branch first add all variations
```
git add .
```
And then commit those variations
```
git commit -m "message"
```
Finally push those commits to github
```
git push -u origin email-field
```

With the alterations done and making sure this version is safe, go back to the master branch
```
git checkout master 
```
Then merge the two branches
```
git merge email-field
```
And tag it
```
git tag v1.3.0
git push origin v1.3.0
```

With the second part, on the other branch "fix-invalid-email", repeat the previous steps of the branch part with its alterations to the code.
Git is very direct and easy version control system.

Jenkins Demo
===========
##CA5/Part1

Download jenkins war file.
On the command line write 
```
java -jar jenkins.war
```
on the folder with the jenkins.war

After running the command, go to your browser and open localhost:8080
It will open a Jenkins page to access an account, enter you account username and password.
Create a job in jenkins, choose type pipeline, for now use the pipeline script without SCM.
Use various stages:

    pipeline {
        agent any

        stages {
            stage('Checkout') {
                steps {
                    echo 'Checking out...'
                    git 'https://andre_leal_1191750@bitbucket.org/andre_leal_1191750/devops-19-20-a-1191750.git'
                }
            }
            stage('Assemble') {
                steps{
                    dir('ca2/ca2.1') {
                        echo 'Assembling...'
                        bat './gradlew build -x test '
                    }
                }
            }
            stage('Test') {
                steps {
                    dir('ca2/ca2.1') {
                        echo 'Testing...'
                        bat './gradlew test'
                        bat 'echo > build/test-results/test/.xml'
                        junit 'build/test-results/test/.xml'
                    }
                }
            }
            stage('Archiving') {
                steps {
                    dir('ca2/ca2.1'){
                        echo 'Archiving...'
                        archiveArtifacts 'build/distributions/'
                    }
                }
            }
        }
    }
Adapt the code to the chosen repository and change the directories to the ones with the required files in th repository

Save the configuration and click on "Build now"
If nothing is wrong, each box bellow each stage should appear green. 
Change pipeline script to pipeline script SCM with the wanted repository to create Jenkinsfile.

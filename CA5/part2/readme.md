Jenkins demo
=========
##CA5/part2

For the part2, do the same thing done in the first part but change the directories to ca2 part2
Some new stages are made. The first is html publish which is a plugin needed to be added to your jenkins.
        
    publishHTML ([
                reportName: 'Javadoc',
              reportDir: 'build/docs/javadoc/',
              reportFiles: 'index.html',
              keepAll: false,
              alwaysLinkToLastBuild: false,
              allowMissing: false
             ])

Now to the stages of docker
    
    stage('Docker Image') {
                steps {
                    dir('ca5/part2') {
                        script {
                            dockerimage = docker.build("andreleal1191750/ca5:${env.BUILD_ID}")
                        }
                    }
                }
            }
            stage('Publish Image') {
                steps {
                    dir('ca5/parte2') {
                        script{
                            bat 'docker login -u="andreleal1191750" -p="passworddocker"'
                            dockerimage.push()
                        }
                    }
                }
            }

Change the directories and docker login to the pretended ones.
Save and Build, its should all be ok.

#Alternative

In Buddy you can build, test and deploy on push in seconds. Git platform for web and software developers with Docker-based tools for Continuous Integration and Deployment.

Buddy compared to Jenkins is very user friendly although not every thing you can do on jenkins can be done on Buddy.

Creating actions is way easier, just choose from a lot of choices, in this case it'll be used Graddle actions

    cd ca2/part2
    echo "Building..."
    gradle clean assemble 

For the action Test:

    cd ca2/part2   
    echo 'Testing...'
    gradle test

For generating Javadoc:

    cd ca2/part2
    echo 'Generating javadocs...'
    gradle javadoc

To build the docker image, choose the action build image:

ca5/part2/Dockerfile

To push the image previously created it will ask for docker login and password, also on the tag use $BUDDY_EXECUTION_ID
Save the actions and Run the pipeline, if any error occurs buddy lets you change that action and run it from there.
Extract the pipeline to an .yml file so that anybody can use it.

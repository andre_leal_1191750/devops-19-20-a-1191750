Gradle Basic Demo
===================

This is a demo application that implements a basic multithreaded chat room server.

The server supports several simultaneous clients through multithreading. When a client connects the server requests a screen name, and keeps requesting a name until a unique one is received. After a client submits a unique name, the server acknowledges it. Then all messages from that client will be broadcast to all other clients that have submitted a unique screen name. A simple "chat protocol" is used for managing a user's registration/leaving and message broadcast.


Prerequisites
-------------

 * Java JDK 8
 * Apache Log4J 2
 * Gradle 6.6 (if you do not use the gradle wrapper in the project)
   

Build
-----

To build a .jar file with the application:

    % ./gradlew build 

Run the server
--------------

Open a terminal and execute the following command from the project's root directory:

    % java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp <server port>

Substitute <server port> by a valid por number, e.g. 59001

Run a client
------------

Open another terminal and execute the following gradle task from the project's root directory:

    % ./gradlew runClient

The above task assumes the chat server's IP is "localhost" and its port is "59001". If you whish to use other parameters please edit the runClient task in the "build.gradle" file in the project's root directory.

To run several clients, you just need to open more terminals and repeat the invocation of the runClient gradle task

Start
------
1 - Download and commit to repository https://bitbucket.org/luisnogueira/gradle_basic_demo/. 

2 - Do the previous commands learned on point Build, Run the Server and Run a Client.

3 - Add a task in the gradle.build to run the server, similar to the one used to run a client, and commit.

3.1 - Add junit dependecie for 4.12

4 - Add a task to copy files from src to a backup folder

task copyDocs(type: Copy) {
    from 'src/main/doc'
    into 'build/target/doc'
}

5 - Add a task to zip a file, similar to point 4

6 - tag your repository like so:
$ git tag -a ca2-part1 -m "my version ca2-part1"

6.1 verify if tagged : 
$ git tag

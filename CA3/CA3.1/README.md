Virtualization Demo
===================
-Start by creating a Virtual Machine;

- The VM has 2048 MB RAM, the Network Adapter 2 is set as Host-only Adapter (vboxnet0).

- The networking was configured by using the following commands:

```bash
sudo apt install net-tools
```

- And then: 

```bash
sudo nano /etc/netplan/01-netcfg.yaml
```

- The contents of the file need to be changed as so:

            network:
            version: 2
            renderer: networkd
            ethernets:
            enp0s3:
              dhcp4: yes
            enp0s8:
              addresses: [192.168.56.100/24]
            
- Then, to apply the changes done to the file:

```bash
sudo apt install openssh-server
```

```bash
sudo apt install vsftpd
```

- To enable write access for vsftpd

```bash
sudo nano /etc/vsftpd.conf
 ```
 - Erase th # from the line *write_enable=YES* and restarting the service by:
 
```bash
sudo service vsftpd restart
```

### Part 1

- Cloning the individual repository on the virtual machine:

```git
git clone https://andre_leal_1191750@bitbucket.org/andre_leal_1191750/devops-19-20-a-1191750.git
```

## Running the maven project
-Maven needs to be installed with ```sudo apt install maven```.
Navigating to the project's directory and running ```mvn gwt:run```. 

- As it is to be expected, it failed while resolving dependencies since some proper tools to run maven's gwt:run are missing, like the proper java jdk.

##Gradle projects

#### CA 2.1

- Navigating to the folders where gradle projects were by using the cd command.

- Installing gradle by running ```sudo apt install gradle```.

- Then, experimenting some gradle commands to run the projects: 

```gradle
./gradlew build
```

- The build was successful. 

```
./gradlew copyDocs
./gradlew zipFile
```

#### CA 2.2 

- Navigating to the correspondent folder in part 2, by using the cd command. Then, running:

```gradle
./gradlew deleteWebpackFiles
./gradlew copyJar
```

### Dificulties may be found

When creating VM, before starting it, might need to type
```
bcdedit /set hypervisorlaunchtype off
```
On cmd line of the "Real" machine, to start the Ubuntu installation.

Also, to stop from always being installing Ubuntu and not opening th machine:
On the machine settings, on the system part, put the hard disk in first, then optical disk and disenable Floppy.
                                                                                                               

Vagrant Demo
============

CA3 part 2

  Install Vagrant and VirtualBox in you computer.

## Steps

### 1

Create a local folder in your computer and copy the Vagrantfile into that folder

### 2

Execute 
```
vagrant up
```

This will create two vagrant VMs (i.e, "db" and "web"). The first run will take some time because both machines will be provisioned with several software packages.

### 3

In the host you can open the spring web application using one of the following options:

  - http://localhost:8080/basic-0.0.1-SNAPSHOT/
  - http://192.168.33.10:8080/basic-0.0.1-SNAPSHOT/

You can also open the H2 console using one of the following urls:

- http://localhost:8080/basic-0.0.1-SNAPSHOT/h2-console
- http://192.168.33.10:8080/basic-0.0.1-SNAPSHOT/h2-console

For the connection string use: jdbc:h2:tcp://192.168.33.11:9092/./jpadb

### 4 

Copy the vagrant file into your repository

### 5

Update the Vagrantfile configuration so that it uses your own gradle version of the spring application
line 69:

```
# Change the following command to clone your own repository!
      sudo git clone https://andre_leal_1191750@bitbucket.org/andre_leal_1191750/devops-19-20-a-1191750.git
      cd CA2/CA2.2
      sudo chmod u+rwx gradlew
```

### 6

Change necessary code so that the spring application uses the H2 server in the db VM

build.gradle 

line 6 
```
id 'war'
```
line 26
```
// To support war file for deploying to tomcat
providedRuntime 'org.springframework.boot:spring-boot-starter-tomcat'
```

create src/main/java/com/greglturnquist/payroll/ServletInitializer.java

```
package com.greglturnquist.payroll;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
public class ServletInitializer extends SpringBootServletInitializer {
   @Override
   protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
       return application.sources(ReactAndSpringDataRestApplication.class);
   }
}
```
mofify  src/main/js/app.js
```
-		client({method: 'GET', path: '/api/employees'}).done(response => {
+		client({method: 'GET', path: '/basic-0.0.1-SNAPSHOT/api/employees'}).done(response => {
```

modify  src/main/resources/application.properties 
```
-spring.data.rest.base-path=/api
+server.servlet.context-path=/basic-0.0.1-SNAPSHOT
+spring.data.rest.base-path=/api
+#spring.datasource.url=jdbc:h2:mem:jpadb
+# In the following settings the h2 file is created in /home/vagrant folder
+spring.datasource.url=jdbc:h2:tcp://192.168.33.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
+spring.datasource.driverClassName=org.h2.Driver
+spring.datasource.username=sa
+spring.datasource.password=
+spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
+# So that spring will no drop de database on every execution.
+spring.jpa.hibernate.ddl-auto=update
+spring.h2.console.enabled=true
+spring.h2.console.path=/h2-console
+spring.h2.console.settings.web-allow-others=true
```

modify src/main/resources/templates/index.html
```
-    <link rel="stylesheet" href="/main.css" />
+    <link rel="stylesheet" href="main.css" />
```

### 7 

Repeat step 2 and 3 to see if everything is running